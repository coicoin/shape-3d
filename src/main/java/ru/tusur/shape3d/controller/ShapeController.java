package ru.tusur.shape3d.controller;

import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Shape3D;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;
import ru.tusur.shape3d.ShapeApplication;

import java.io.IOException;
import java.util.Objects;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static java.util.Objects.nonNull;

public class ShapeController {

    @FXML
    private MenuItem boxBlueColorButton;

    @FXML
    private MenuItem boxDefaultColorButton;

    @FXML
    private MenuItem boxGreenColorButton;

    @FXML
    private MenuItem boxRedColorButton;

    @FXML
    private CheckMenuItem boxRotateCheckbox;

    @FXML
    private Box boxShape;

    @FXML
    private MenuItem closeButton;

    @FXML
    private MenuItem cylinderBlueColorButton;

    @FXML
    private MenuItem cylinderDefaultColorButton;

    @FXML
    private MenuItem cylinderGreenColorButton;

    @FXML
    private MenuItem cylinderRedColorButton;

    @FXML
    private CheckMenuItem cylinderRotateCheckbox;

    @FXML
    private Cylinder cylinderShape;

    @FXML
    private Label destinationPoint;

    @FXML
    private MenuItem restartButton;

    @FXML
    private MenuItem sphereBlueColorButton;

    @FXML
    private MenuItem sphereDefaultColorButton;

    @FXML
    private MenuItem sphereGreenColorButton;

    @FXML
    private MenuItem sphereRedColorButton;

    @FXML
    private MenuItem sphereGoldColorButton;

    @FXML
    private CheckMenuItem sphereRotateCheckbox;

    @FXML
    private Sphere sphereShape;

    @FXML
    private AnchorPane subRootPanel;

    @FXML
    private TextField xCoordinate;

    @FXML
    private TextField yCoordinate;

    public MenuItem minSpeed;
    public MenuItem middleSpeed;
    public MenuItem maxSpeed;
    public Button circulationButton;
    public Button squareMovingButton;
    public Button freeMovingButton;

    private boolean circulationIsRunning;
    private boolean squareMovingIsRunning;
    private boolean freeMovingIsRunning;
    private boolean isMoved;

    private Timer circulationTimer;
    private TimerTask circulationTimerTask;
    private Timer squareMovingTimer;
    private TimerTask squareMovingTimerTask;
    private Timer freeMovingTimer;
    private TimerTask freeMovingTimerTask;
    RotateTransition boxRotateTransition = new RotateTransition();
    RotateTransition cylinderRotateTransition = new RotateTransition();

    @FXML
    void initialize() {
        TranslateTransition freeMovingTranslateTransition = new TranslateTransition();
        TranslateTransition squareMovingTranslateTransition = new TranslateTransition();
        TranslateTransition circulationMovingTranslateTransition = new TranslateTransition();

        //turn the shapes
        transformShape(sphereShape);
        transformShape(boxShape);
        transformShape(cylinderShape);

        //put shapes into order
        initShapePosition();

        //change shape color
        sphereGoldColorButton.setOnAction(event -> changeShapeColor(Color.GOLD, Color.BLACK, sphereShape));
        sphereBlueColorButton.setOnAction(event -> changeShapeColor(Color.LIGHTSKYBLUE, Color.DARKBLUE, sphereShape));
        sphereGreenColorButton.setOnAction(event -> changeShapeColor(Color.LIGHTGREEN, Color.DARKGREEN, sphereShape));
        sphereRedColorButton.setOnAction(event -> changeShapeColor(Color.RED, Color.PINK, sphereShape));
        sphereDefaultColorButton.setOnAction(event -> sphereShape.setMaterial(null));

        boxBlueColorButton.setOnAction(event -> changeShapeColor(Color.LIGHTSKYBLUE, Color.DARKBLUE, boxShape));
        boxGreenColorButton.setOnAction(event -> changeShapeColor(Color.LIGHTGREEN, Color.DARKGREEN, boxShape));
        boxRedColorButton.setOnAction(event -> changeShapeColor(Color.RED, Color.PINK, boxShape));
        boxDefaultColorButton.setOnAction(event -> boxShape.setMaterial(null));

        cylinderBlueColorButton.setOnAction(event -> changeShapeColor(Color.LIGHTSKYBLUE, Color.DARKBLUE, cylinderShape));
        cylinderGreenColorButton.setOnAction(event -> changeShapeColor(Color.LIGHTGREEN, Color.DARKGREEN, cylinderShape));
        cylinderRedColorButton.setOnAction(event -> changeShapeColor(Color.RED, Color.PINK, cylinderShape));
        cylinderDefaultColorButton.setOnAction(event -> cylinderShape.setMaterial(null));

        //assign speed
        TranslateTransition translateTransition = new TranslateTransition();
        translateTransition.setDuration(Duration.millis(1000));
        minSpeed.setDisable(false);
        middleSpeed.setDisable(true);
        maxSpeed.setDisable(false);
        minSpeed.setOnAction(event -> {
            translateTransition.setDuration(Duration.millis(2000));
            circulationMovingTranslateTransition.setDuration(Duration.millis(1000));
            freeMovingTranslateTransition.setDuration(Duration.millis(1000));
            minSpeed.setDisable(true);
            middleSpeed.setDisable(false);
            maxSpeed.setDisable(false);
        });
        middleSpeed.setOnAction(event -> {
            translateTransition.setDuration(Duration.millis(1000));
            circulationMovingTranslateTransition.setDuration(Duration.millis(500));
            freeMovingTranslateTransition.setDuration(Duration.millis(500));
            minSpeed.setDisable(false);
            middleSpeed.setDisable(true);
            maxSpeed.setDisable(false);
        });
        maxSpeed.setOnAction(event -> {
            translateTransition.setDuration(Duration.millis(100));
            circulationMovingTranslateTransition.setDuration(Duration.millis(100));
            freeMovingTranslateTransition.setDuration(Duration.millis(100));
            minSpeed.setDisable(false);
            middleSpeed.setDisable(false);
            maxSpeed.setDisable(true);
        });

        //get point on mouse click
        subRootPanel.setOnMouseClicked(event -> {
            if (isMoved) {
                xCoordinate.setText(String.valueOf(translateTransition.getToX()));
                destinationPoint.setLayoutX(translateTransition.getToX());
                yCoordinate.setText(String.valueOf(translateTransition.getToY()));
                destinationPoint.setLayoutY(translateTransition.getToY());
                isMoved = false;
            } else {
                xCoordinate.setText(String.valueOf(event.getSceneX()));
                yCoordinate.setText(String.valueOf(event.getSceneY() - 100));
                destinationPoint.setLayoutX(event.getSceneX());
                destinationPoint.setLayoutY(event.getSceneY() - 100);
            }
        });

        sphereRotateCheckbox.setOnAction(event -> {
            if (sphereRotateCheckbox.isSelected()) {
                rotateShape(sphereShape, boxRotateTransition);
            }
        });
        sphereShape.setOnMouseClicked(event -> moveShape(sphereShape, translateTransition));

        boxRotateCheckbox.setOnAction(event -> {
            if (boxRotateCheckbox.isSelected()) {
                rotateShape(boxShape, boxRotateTransition);
            } else {
                boxRotateTransition.stop();
            }
        });
        boxShape.setOnMouseClicked(event -> moveShape(boxShape, translateTransition));

        cylinderRotateCheckbox.setOnAction(event -> {
            if (cylinderRotateCheckbox.isSelected()) {
                rotateShape(cylinderShape, cylinderRotateTransition);
            } else {
                cylinderRotateTransition.stop();
            }
        });
        cylinderShape.setOnMouseClicked(event -> moveShape(cylinderShape, translateTransition));

        restartButton.setOnAction(event -> {
            Stage stage = (Stage) subRootPanel.getScene().getWindow();
            stage.close();
            Platform.runLater(() -> {
                try {
                    new ShapeApplication().start(new Stage());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });

        closeButton.setOnAction(event -> {
            Stage stage = (Stage) subRootPanel.getScene().getWindow();
            stage.close();
        });

        circulationButton.setOnMouseClicked(event -> {
            if (circulationIsRunning) {
                circulationIsRunning = false;
                circulationTimerTask.cancel();
                circulationTimer.cancel();
                circulationTimer.purge();
                circulationButton.setTextFill(Color.WHITE);
                isMoved = false;
            } else {
                double x0 = (subRootPanel.getWidth() - sphereShape.getRadius()) / 2;
                double y0 = (subRootPanel.getHeight() - sphereShape.getRadius() * 2.5) / 2;
                double radius = Math.min(y0, x0);
                circulationMovingTranslateTransition.setNode(sphereShape);
                circulationMovingTranslateTransition.setAutoReverse(true);
                circulationTimer = new Timer();

                circulationTimerTask = new TimerTask() {
                    double angle = 0;

                    @Override
                    public void run() {
                        double x, y;
                        x = x0 + radius * Math.cos(angle);
                        y = y0 + radius * Math.sin(angle);
                        circulationMovingTranslateTransition.setToX(x);
                        circulationMovingTranslateTransition.setToY(y);
                        circulationMovingTranslateTransition.play();
                        angle += 0.05;
                    }
                };
                circulationTimer.schedule(circulationTimerTask, 0, 70);
                circulationIsRunning = true;
                circulationButton.setTextFill(Color.LIGHTSEAGREEN);
            }

        });

        squareMovingButton.setOnMouseClicked(event -> {
            if (squareMovingIsRunning) {
                squareMovingIsRunning = false;
                squareMovingButton.setTextFill(Color.WHITE);
                squareMovingTranslateTransition.stop();
                squareMovingTimer.cancel();
                squareMovingTimerTask.cancel();
            } else {
                double[] pointA = {subRootPanel.getWidth() - boxShape.getWidth() * 1.8, 0.0};
                double[] pointB = {subRootPanel.getWidth() - boxShape.getWidth() * 1.8, subRootPanel.getHeight() - boxShape.getWidth() * 1.6};
                double[] pointC = {0.0, subRootPanel.getHeight() - boxShape.getWidth() * 1.6};
                double[] pointD = {0.0, 0.0};
                double[][] arr = {pointA, pointB, pointC, pointD};
                squareMovingTimer = new Timer();
                squareMovingTranslateTransition.setNode(boxShape);
                squareMovingTranslateTransition.setAutoReverse(true);
                subRootPanel.requestLayout();

                squareMovingTimerTask = new TimerTask() {
                    int i = 0;

                    @Override
                    public void run() {
                        squareMovingTranslateTransition.setToX(arr[i][0]);
                        squareMovingTranslateTransition.setToY(arr[i][1]);
                        squareMovingTranslateTransition.play();
                        if (i == arr.length - 1) {
                            i = 0;
                        } else {
                            i++;
                        }
                    }
                };
                squareMovingTimer.schedule(squareMovingTimerTask, 0, 100);
                squareMovingIsRunning = true;
                squareMovingButton.setTextFill(Color.LIGHTSEAGREEN);
            }
        });

        freeMovingButton.setOnMouseClicked(event -> {
            if (freeMovingIsRunning) {
                freeMovingIsRunning = false;
                freeMovingButton.setTextFill(Color.WHITE);
                freeMovingTranslateTransition.stop();
                freeMovingTimer.cancel();
                freeMovingTimerTask.cancel();
            } else {
                Random random = new Random();
                freeMovingTimer = new Timer();
                freeMovingTranslateTransition.setNode(cylinderShape);

                freeMovingTimerTask = new TimerTask() {
                    @Override
                    public void run() {
                        double x = random.nextInt((int) subRootPanel.getWidth());
                        if (x < 0.0) {
                            x = 0.0;
                        } else if (x > subRootPanel.getWidth() - cylinderShape.getHeight()) {
                            x = subRootPanel.getWidth() - cylinderShape.getHeight();
                        }
                        double y = random.nextInt((int) subRootPanel.getHeight());
                        if (y < 0.0) {
                            y = 0.0;
                        } else if (y > subRootPanel.getHeight() - cylinderShape.getHeight()) {
                            y = subRootPanel.getHeight() - cylinderShape.getHeight();
                        }
                        freeMovingTranslateTransition.setToX(x);
                        freeMovingTranslateTransition.setToY(y);
                        freeMovingTranslateTransition.setAutoReverse(true);
                        subRootPanel.requestLayout();
                        freeMovingTranslateTransition.play();
                    }
                };
                freeMovingTimer.schedule(freeMovingTimerTask, 0, 1000);
                freeMovingIsRunning = true;
                freeMovingButton.setTextFill(Color.LIGHTSEAGREEN);
            }
        });
    }

    private void changeShapeColor(Color diffuseColor, Color specularColor, Shape3D shape3D) {
        PhongMaterial material = new PhongMaterial();
        material.setDiffuseColor(diffuseColor);
        material.setSpecularColor(specularColor);
        shape3D.setMaterial(material);
    }

    private void rotateShape(Node shape, RotateTransition rotateTransition) {
        //Creating a rotated transition
        rotateTransition.setDuration(Duration.millis(1000));
        rotateTransition.setCycleCount(1000);
        rotateTransition.setNode(shape);
        rotateTransition.setByAngle(360);
        rotateTransition.setAutoReverse(false);
        rotateTransition.play();
    }

    private void moveShape(Node shape, TranslateTransition translateTransition) {
        //Creating a moving transition
        translateTransition.setNode(shape);
        if (nonNull(xCoordinate.getText()) && !Objects.equals(xCoordinate.getText(), "")) {
            if (Double.parseDouble(xCoordinate.getText()) < 0) {
                translateTransition.setToX(Double.parseDouble(String.valueOf(0.0)));
            } else
                translateTransition.setToX(Math.min(Double.parseDouble(xCoordinate.getText()), subRootPanel.getWidth() - 100));
        } else {
            translateTransition.setToX(0.0);
            xCoordinate.setText(String.valueOf(translateTransition.getByX()));
        }
        if (nonNull(yCoordinate.getText()) && !Objects.equals(yCoordinate.getText(), "")) {
            if (Double.parseDouble(yCoordinate.getText()) < 0) {
                translateTransition.setToY(Double.parseDouble(String.valueOf(0.0)));
            } else
                translateTransition.setToY(Math.min(Double.parseDouble(yCoordinate.getText()), subRootPanel.getHeight() - 100));
        } else {
            translateTransition.setToY(0.0);
            yCoordinate.setText(String.valueOf(translateTransition.getByY()));
        }
        translateTransition.play();
        isMoved = true;
    }

    private void transformShape(Node shape) {
        //Creating the translation transformation to turn the shape
        Rotate rxBox = new Rotate(0, 0, 0, 0, Rotate.X_AXIS);
        Rotate ryBox = new Rotate(0, 0, 0, 0, Rotate.Y_AXIS);
        Rotate rzBox = new Rotate(0, 0, 0, 0, Rotate.Z_AXIS);
        rxBox.setAngle(30);
        ryBox.setAngle(50);
        rzBox.setAngle(30);
        shape.getTransforms().addAll(rxBox, ryBox, rzBox);
    }

    private void initShapePosition() {
        TranslateTransition translateTransitionInitBoxShape = new TranslateTransition();
        translateTransitionInitBoxShape.setNode(boxShape);
        translateTransitionInitBoxShape.setToX(0.0);
        translateTransitionInitBoxShape.setToY(80.0);
        translateTransitionInitBoxShape.play();
        TranslateTransition translateTransitionInitCylinderShape = new TranslateTransition();
        translateTransitionInitCylinderShape.setNode(cylinderShape);
        translateTransitionInitCylinderShape.setToX(10);
        translateTransitionInitCylinderShape.setToY(110.0 + boxShape.getHeight());
        translateTransitionInitCylinderShape.play();
    }

}